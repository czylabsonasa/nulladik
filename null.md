# A Julia beszerzése

## Helyi Julia telepítése LINUX-on
A következőket az:
```bash
$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.2 LTS
Release:	18.04
Codename:	bionic
```
os-en teszteltem<br>
Ez a módszer a felhasználói könyvtárba telepít. Így jobban követhetjük a verziókat. 

Nyissunk egy terminált és mezei felhasználóként adjuk ki a:
```bash
mkdir .local
cd ~/.local
wget https://julialang-s3.julialang.org/bin/linux/x64/1.1/julia-1.1.0-linux-x86_64.tar.gz
tar xzf julia-1.1.0-linux-x86_64.tar.gz
ln -s julia-1.1.0 julia # így könnyen váltogathatjuk
echo >> ~/.bashrc # ha nincs sorvége a fájl végén
echo 'export PATH=~/.local/julia/bin:$PATH' >> ~/.bashrc
source ~/.bashrc
```
parancsokat (copy-paste). Ez a parancssorozat az 1.1 verziót teszi fel. 
## Csomagok telepítése (kívülről):
```bash
julia <<< '
import Pkg
Pkg.add("DataStructures")
Pkg.add("OffsetArrays")
Pkg.add("Plots")
Pkg.add("LaTeXStrings")
'
```
vagy a REPL-ben (belülről) ugyanígy.

## Előtelepített LINUX lemezkép használata

Elkészítettem egy Ubuntu 18.04-es képet, Julia, Jupyter, VSCode előtelepítve. Letölthető 
a [innen](https://drive.google.com/file/d/1cn1PrgfRM0Oo2hq_Qp2EGOavMldOCMCe/view?usp=sharing).

Felhasználónév: **julia**<br>
Jelszó: **julia123***


## Távoli használat - JuliaBox

A [juliabox.com](https://juliabox.com/) helyen válasszuk a *free* előfizetést. 
Regisztráció után a rendszer használható. Előnye: csak egy böngésző kell hozzá. Hátrány: időlimit.


# Szerkesztő

Bármilyen sima szöveges szerkesztő megfelel. Az egyik jó Julia támogatottságú:
## A VsCode telepítése: [innen](https://code.visualstudio.com/Download).
Miután elindítottuk telepítsük a Julia Language Support (pl a szintaktikus szövegkiemelés miatt)
![](pub/pre.png)


