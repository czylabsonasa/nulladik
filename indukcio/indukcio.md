### [Teljes indukció](https://matekarcok.hu/bizonyitasi-modszerek/)

### Feladat:
Van-e olyan $`N`$, mellyel $`2^n>n^2`$ ha $`n>N`$?

Nézzük meg géppel, hogy mi történik az elején:

```julia
using Printf
for n in 0:10
  @printf("%-4d%10d\n",n,2^n-n^2)
end
```
Látjuk, hogy $`n\ge 5`$ esetén a baloldal nagyobb mint a jobb.
Az a sejtésünk, hogy innen kezdve ez mindig fennáll.
Öröklődik-e, ha igen hogyan és miért az egyenlőtlenség pl. $`n=100`$-ról $`101`$-re...?
Tegyük fel, hogy $`n\ge 5`$ és $`2^n > n^2`$. Ekkor
```math
2^{n+1}=2\cdot 2^{n} \stackrel{\text{feltevés}}{>} 2\cdot n^2 \stackrel{?}{\ge} (n+1)^2
```
A kérdőjeles egyenlőtlenség, igaz mert: $`(n-1)^2 \ge 2`$.
Ezzel beláttuk, hogy
```math
2^{n} > n^2 \\ \text{ minden } n\ge 5 \text{ -re}
```

#### Házi feladat:
Vizsgáljuk meg az $`2^n>n^3`$ egyenlőtlenséget az előbbi módon!


#### Feladat:
Van-e olyan $`N`$, mellyel $`n!>2^n`$ ha $`n>N`$?

Nézzük meg géppel, hogy mi történik az elején:
```julia
for n in 1:20
  @printf("%-4d%20d\n",n,prod(1:n)-2^n)
end
```
Látjuk, hogy $n\ge 4$ esetén a baloldal nagyobb mint a jobb.
Az a sejtésünk, hogy innen kezdve ez mindig fennáll.
Öröklődik-e, ha igen hogyan és miért az egyenlőtlenség pl. $`n=1000`$-ről $`1001`$-re...?
Tegyük fel, hogy $`n\ge 4`$ és $`n! > 2^n`$. Ekkor
```math
(n+1)!=(n+1)\cdot n! \stackrel{\text{feltevés}}{>} (n+1)\cdot 2^n \stackrel{n+1>2}{>} 2^{n+1}
```
Azaz, beláttuk, hogy
```math
n! > 2^n \ \ \text{ minden } n>3 \text{ -re }
```



#### Julia:
a [printf](https://docs.julialang.org/en/v1.1/stdlib/Printf/#Printf-1) használata
