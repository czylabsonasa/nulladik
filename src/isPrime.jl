function isPrime(n)
  if n<5 return (false,true,true,false)[n] end
  if 0==mod(n,2) || 0==mod(n,3) return false end
  d,delta=5,2
  while d*d<=n # itt nekünk kell kezelni a ciklusváltozót
    if 0==mod(n,d) return false end
    d+=delta
    delta=6-delta
  end
  true
end
