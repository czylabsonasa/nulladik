function solve()
  n,k,m=parse.(Int,split(readline()))
  a=sort(parse.(Int,split(readline())))
  d,i=0,1
  while i<=n && k>0
    if m>=a[i] 
      k-=1; 
      d+=1; 
    else
      q,r=divrem(a[i],m)
      q= r>0 ? q+1 : q
      if k>=q
        k-=q
        d+=1
      else
        break
      end
    end
    i+=1
  end
  println(d)
end
solve()