# Frequency Queries


let
  d=Dict{Int,Int}()
  f=Dict{Int,Int}()
  function berak(x)
    if haskey(d,x)
      dx=d[x]
      f[dx]-=1
      dx+=1
      d[x]=dx
      f[dx]=if haskey(f,dx) f[dx]+1 else 1 end
    else
      d[x]=1
      f[1]=if haskey(f,1) f[1]+1 else 1 end
    end
  end
  function kivesz(x)
    if haskey(d,x) && d[x]>0 
      dx=d[x]
      f[dx]-=1
      dx-=1
      d[x]=dx
      f[dx]=if haskey(f,dx) f[dx]+1 else 1 end
    end
  end

  q=parse(Int,readline())
  for _ in 1:q
  # println("d=",d)
  # println("f=",f)
    op,num=parse.(Int,split(readline()))
    if op==1 
      berak(num)
      continue
    end
    if op==2
      kivesz(num)
      continue
    end
    println( if haskey(f,num) && f[num]>0 1 else 0 end )
  end
end