function medi(arr,lo,up)
println(stderr,arr[lo:up])    
  nn=up-lo+1  
  if 1==nn return arr[lo] end
  if 2==nn return 0.5*(arr[lo]+arr[up]) end
  mm=lo+div(nn,2)
  if 1==mod(nn,2)
    return arr[mm]
  end
  0.5*(arr[mm-1]+arr[mm])
end
  
function solve()

  n=parse(Int,readline())
  x=sort(parse.(Int,split(readline())))
  
  q1,q2,q3=0,0,0
  
  q2=medi(x,1,n)
  m=1+div(n,2)
  if 0==mod(n,2)
    q1=medi(x,1,m-1)
    q3=medi(x,m,n)
  else
    q1=medi(x,1,m-1)
    q3=medi(x,m+1,n)
  end

  function ir(x)
    xx=Int(floor(x))
    print(xx)
    if xx<x print(".5") end
    println() 
  end
  ir(q1)
  ir(q2)
  ir(q3)

end

solve()