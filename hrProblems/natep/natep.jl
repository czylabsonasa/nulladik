function solve()
  function best(frek)
    s,fopt,xopt=sum(frek),-1,'_'
    for c in Int('z'):-1:Int('a')
      t=s-frek[c]
      if t>fopt
        xopt=c
        fopt=t
      end
    end
    xopt
  end
  fr=fill(0,128,5)
  n=parse(Int,readline())
  for _ in 1:n
    szo=readline()
    for i in 1:5
      fr[Int(szo[i]),i]+=1
    end
  end
  ans=[]
  for i in 1:5
    push!(ans,Char(best(fr[:,i])))
  end
  println(join(ans))
end
solve()
