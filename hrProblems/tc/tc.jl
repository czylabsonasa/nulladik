function mer(x1,x2) # simulate "strict", alternating merge 
  if x1[1]>x2[1] x1,x2=x2,x1 end # x1 a kezdő
  n1,n2=length(x1),length(x2)
  if n2>n1 || n1-n2>1 return false end
  pr=x1[1]-1
  for i in 1:min(n1,n2)
    if x1[i]<=pr || x1[i]>=x2[i] return false end
    pr=x2[i]
  end
  if n1>n2 && x1[n1]<=x2[n2] return false end
  true
end

function pre(s)
  d=Dict{Char,Vector{Int}}()
  for i in 1:length(s)
    si=s[i]
    if haskey(d,si)
      push!(d[si],i)
    else
      d[si]=[i]
    end
  end
  d
end

function solve(d)
  ret=0
  chr=[c for c in collect('a':'z') if true==haskey(d,c)]
  for i in 1:length(chr)-1 
    di=d[chr[i]]
    li=length(di)
    for j in (i+1):length(chr)
      dj=d[chr[j]]
      lj=length(dj)
      if lj+li>ret && mer(di,dj)==true
        ret=lj+li
# println(ret," ",chr[i], " ", chr[j] )
# println(li, " ", lj )

      end
    end
  end
  ret
end


readline()
println(solve(pre(readline())))


