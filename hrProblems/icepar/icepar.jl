# Hash Tables: Ice Cream Parlor

let
  T=parse(Int,readline())
  for _ in 1:T
    mon=parse(Int,readline())
    n=parse(Int,readline())
    c=parse.(Int,split(readline()))
    d=Dict{Int,Int}()
    ans=nothing
    for i in 1:n
      ci=c[i]
      if false==haskey(d,ci)
        d[ci]=i
      else
        if 2*ci == mon
          ans=[d[ci],i]; 
          #println(d[ci],"...",i)
          break
        end
      end
    end
    if ans==nothing
      for (k,v) in d
        if 2*k!= mon && haskey(d,mon-k)
          ans=sort([v,d[mon-k]])
          break
        end
      end
    end
    println(ans[1]," ",ans[2])
  end

end