function solve()
  _,d=parse.(Int,split(readline()))
  h=unique(sort(parse.(Int,split(readline()))))
  n=length(h)
  function keres(i,val)
    while i<=n && h[i]<=val
      i+=1
    end
    i
  end
  i,ntr=1,0
  while i<=n
    i=keres(i+1,h[i]+d)
    ntr+=1 
    if i>n break end
    i=keres(i,h[i-1]+d)
  end
  println(ntr)
end
solve()