# Maximum Subarray Sum
# nem jo az oda vissza scan
function solve(arr,m)
  mx,mn,s=0,m+1,0
  for i in 1:length(arr)
    s=(s+arr[i])%m
#println(i," ",s)
    if s>mx mx=s end
    if s<mx mx=max(s+m-mx,mx) end
    if s>mn mx=max(mx,s-mn) end
    if s<mn mn=s end
    if 0==mx println("0mx") end
  end
  mx
end

let
  Q=parse(Int,readline())
  for _ in 1:Q
    n,m=parse.(Int,split(readline()))
    arr=parse.(Int,split(readline()))
    mx1=solve(arr,m)
    # arr .= (m .- arr)
    # arr[arr.==m] .= 0
    reverse!(arr)
    mx2=solve(arr,m)
    println(max(mx1,mx2))
  end
end
