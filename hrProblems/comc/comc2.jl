function solve(fn)
  fh=open(fn,"r")
  sa=Int.(collect(readline(fh)))
  sb=Int.(collect(readline(fh)))
  close(fh)
  function van(s,N=128)
    v=fill(false,N)
    for si in s
      v[Int(si)]=true
    end
    v
  end
  v = van(sa) .* van(sb) 
  a=vcat(-1,[c for c in sa if v[c]])
  b=vcat(-2,[c for c in sb if v[c]])
  la,lb=length(a),length(b)
  ans=0
  if la>0 && lb>0
    prev,akt=zeros(Int,lb),zeros(Int,lb)
    function belso(ai,j)
      if ai==b[j]
        akt[j]=1+prev[j-1]
      else
        akt[j]=max(akt[j-1],prev[j])
      end
    end
    for i in 2:la
      prev,akt=akt,prev
      akt[1]=0
      ai=a[i]
      for j in 2:lb
        belso(ai,j)
        # if a[i]==b[j]
        #   akt[j]=1+prev[j-1]
        # else
        #   akt[j]=max(akt[j-1],prev[j])
        # end
      end
    end
    ans=akt[lb]
  end
  println(ans)
end
