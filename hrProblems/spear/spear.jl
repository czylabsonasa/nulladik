let
  n=parse(Int,readline())
  function pre()
    X=parse.(Float64,split(readline()))
    X=sort([[X[i],i] for i in 1:n],by=(u)->u[1])
    for i in 1:n X[i][1]=i end
    sort(X,by=(u)->u[2])
  end
  x=pre();y=pre()
  s=0.0
  for i in 1:n
    s+=(x[i][1]-y[i][1])^2
  end
  println(round(1-6.0*s/((n-1)*n*(n+1)),digits=3))
end