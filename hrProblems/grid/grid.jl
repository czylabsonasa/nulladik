function solve()
  n,m,k=parse.(Int,split(readline()))
  tr=[]
  for i in 1:k
    push!(tr,parse.(Int,split(readline())))
  end
  sort!(tr)
  s=0
  pr=-1 # elozo sor 
  pb=-1 # elozo vegallomas 
  for i in 1:k
    r,a,b=tr[i]
    if r>pr 
      pr,pb=r,b
      s+=(b-a+1)
      continue
    end
    if a>pb 
      s+=(b-a+1)
      pb=b
      continue
    end

    if b<=pb continue end
    s+=b-pb;
    pb=b
  end


  println(n*m-s)
end
solve()