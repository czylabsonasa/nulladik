function solve()
  n=parse(Int,readline())
  start=Set(collect('a':'z'))
  while n>0 && !isempty(start)
    start=intersect(start,Set(readline()))
    n-=1
  end
  println(length(start))
end

solve()