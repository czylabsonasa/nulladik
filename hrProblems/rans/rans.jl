# Hash Tables: Ransom Note
let
  m,n=parse.(Int,split(readline()))
  d=Dict{String,Int}()
  for w in split(readline())
    if haskey(d,w)
      d[w]+=1
    else
      d[w]=1
    end
  end
  ans="Yes"
  for w in split(readline())
    if haskey(d,w) && d[w]>0
      d[w]-=1
    else
      ans="No"
      break
    end
  end
  println(ans)
end