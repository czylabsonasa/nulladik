function activityNotifications(expenditure, d)
    counts = zeros(201)
    notifications = 0
    d_even = d % 2 == 0
    m = trunc(Int32, d/2) + !d_even
    for i in 1:length(expenditure)
        if i > d
            median = 0
            nums = 0
            j = 1
            
            while median == 0 || d_even
                if nums + counts[j] >= m && median == 0
                    median = j-1
                end
                if d_even && nums + counts[j] > m
                    median = (median + j-1) / 2
                    break
                end
                nums += counts[j]
                j += 1
            end
            
            notifications += expenditure[i] >= 2 * median
            counts[expenditure[i-d]+1] -= 1
        end
        counts[expenditure[i]+1] += 1
    end
    return notifications
end

fptr = open("o", "w")

nk = [parse(Int32, k) for k in split(readline())]
n = nk[1]
d = nk[2]

arr = [parse(Int32, k) for k in split(readline())]

res = activityNotifications(arr, d)

write(fptr, string(res)*"\n")

close(fptr)