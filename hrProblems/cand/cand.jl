function solve()
  n=parse(Int,readline())
  a=zeros(Int,n)
  for i in 1:n
    a[i]=parse(Int,readline())
  end
  b=[[a[i],i] for i in 1:n ]
  sort!(b)

  c=zeros(Int,n)
  for (v,j) in b
    #println(v," ",j)
    m=if j>1 && v>a[j-1] c[j-1] else 0 end
    m=max(if j<n && v>a[j+1] c[j+1] else 0 end, m)
    c[j]=m+1
  end
  # println(a)
  # println(c)

  println(sum(c))

end
solve()