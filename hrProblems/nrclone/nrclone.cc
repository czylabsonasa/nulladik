// Find the nearest clone
// bfs from multiple vertices

#include <bits/stdc++.h>
using namespace std;

int main(){
  int const INF=1000000000;
  typedef vector<int> ivec;

  int nodes, edges; cin >> nodes >> edges;
  ivec gr[nodes+1]; // 1 based
  for(int i=0;i<edges;i++){
    int a,b; cin >> a >> b;
    gr[a].push_back(b); 
    gr[b].push_back(a); 
  }
  ivec colors(nodes+1);
  for(int i=1;i<=nodes;i++){
    cin >> colors[i];
  }
  int tCol; cin>>tCol;
  vector<int> dist(nodes+1,INF);
  vector<int> orig(nodes+1,-1);
  vector<int> queue(nodes+1);
  int head=0,tail=0;
  for(int i=1;i<=nodes;i++){
    if(tCol==colors[i]){
      dist[i]=0;
      orig[i]=i;
      queue[tail++]=i;
    }
  }
  int ans=-1;
  if(tail>1){
    while(head<tail){
      int a=queue[head++];
      int da=1+dist[a], oa=orig[a];
      for(auto b:gr[a]){
        int db=dist[b], ob=orig[b];
        if(oa==ob){continue;}
        if(db==INF){
          dist[b]=da; orig[b]=oa;
          queue[tail++]=b;
        }else{
          ans=da+db; head=tail+1; break;
        }
      }
    }
  }
  cout<<ans<<"\n";

  return 0;
}
