function solve()
  n=parse(Int,readline())
  nums,strs=zeros(Int,n),[]
  for i in 1:n
    sor=split(readline())
    nums[i]=parse(Int,sor[1])
    push!(strs, sor[2])
  end 
  p=sortperm(nums)
  # return 0
  # out=""
  # n2=div(n,2)
  # for i in 1:n
  #   if p[i]<=n2 
  #     out=out*"- "
  #   else 
  #     out=out*strs[p[i]]*" "
  #   end
  # end
  # println(out[1:end-1])    
  n2=div(n,2)
  for i in 1:n-1
    if p[i]<=n2 
      print("- ")
    else 
      print(strs[p[i]]," ")
    end
  end
  if p[n]<=n2 
    println("-")
  else 
    println(strs[p[n]])
  end
end
solve()
