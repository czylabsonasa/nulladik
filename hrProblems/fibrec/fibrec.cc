#include <iostream>

using namespace std;

int fibonacci(int n) {
  if(n<2){return n;}
  int a=0,b=1,k=2;
  for(int k=2;k<=n;k++){
    int c=a+b;
    a=b;b=c;
  }
  return b;
}

int main() {
    int n;
    cin >> n;
    cout << fibonacci(n);
    return 0;
}

