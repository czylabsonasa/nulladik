t=parse(Int, readline())
for _ in 1:t
  n=parse(Int, readline())
  B=parse.(Int, split(readline()))
  pL,pU=0,0
  for i in 2:n
    aL=max(B[i-1]-1+pU,pL)   # csak az extrem helyzetueket nezi
    aU=max(B[i]-1+pL,abs(B[i]-B[i-1])+pU)
    pL,pU=aL,aU
  end
  println(max(pL,pU))
end