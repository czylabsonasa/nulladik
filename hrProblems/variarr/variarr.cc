#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,q; cin>>n>>q;
    vector<vector<int>> a(n);
    for(int i=0;i<n;i++){
      int ni; cin>>ni;
      auto& ai(a[i]);
      ai.resize(ni);
      for(int j=0;j<ni;j++){
        cin>>ai[j];
      }
    }
    for(int k=0;k<q;k++){
      int i,j; cin>>i>>j;
      cout<<a[i][j]<<"\n";
    }
    return 0;
}

