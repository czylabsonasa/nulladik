function solve()
  T=parse(Int,readline())
  for _ in 1:T
    n,lim=parse.(Int,split(readline()))
    a=parse.(Int,split(readline()))
    b=parse.(Int,split(readline()))
    sort!(a)
    sort!(b,rev=true)
    if all(a.+b .>= lim)
      println("YES")
    else
      println("NO")
    end
  end
end

solve()