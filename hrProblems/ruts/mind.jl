function freq(s,N)
  f=zeros(Int,N)
  for si in s
    f[Int(si)]+=1
  end
  f
end

function van(s,N=128)
  v=zeros(Int8,N)
  for si in s
    v[Int(si)]=1
  end
  v
end
