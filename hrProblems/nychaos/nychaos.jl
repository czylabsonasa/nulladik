# new year chaos, interview
function Bit(N,v=0) # 1..N
  tree=fill(v,N)
  function init()
    fill!(tree,v)
  end
  function query(x)
    ans=0
    while x>0
      ans+=tree[x]
      x-=(x&(-x))
    end
    ans
  end
  function update(x,val=1)
    while x<=N
      tree[x]+=val
      x+=(x&(-x))
    end
  end
  init,query,update,tree
end

let
  T=parse(Int,readline())
  for _ in 1:T
    n=parse(Int,readline())
    arr=parse.(Int,split(readline()))

    tc=false
    ans="Too chaotic"
    for i in 1:n
      if i + 2 < arr[i] tc=true; break end
    end
    if false==tc
      init,query,update,_=Bit(n)    
      init()
      ans=0
      update(arr[n])
      for i in n-1:-1:1
        ans+=query(arr[i])
        update(arr[i])
      end
    end
    println(ans)
  end
end
