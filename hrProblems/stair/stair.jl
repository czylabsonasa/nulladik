let
  N=36
  pre=zeros(Int,N)
  pre[1:3]=[1,2,4]
  for i in 4:N
    pre[i]=pre[i-1]+pre[i-2]+pre[i-3]
  end
# println(pre[2:N] ./ pre[1:N-1])  
  s=parse(Int, readline())
  for _ in 1:s
    println(pre[parse(Int,readline())])
  end
end