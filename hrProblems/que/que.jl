function solve()
  q=parse(Int,readline())
  que=zeros(Int,q+1)
  head=tail=1
  for _ in 1:q
    ll=parse.(Int,split(readline()))
    if ll[1]==1
      que[tail]=ll[2]; tail+=1
      continue
    end
    if ll[1]==3
      println(que[head])
      continue
    end
    head+=1
  end
end
solve()