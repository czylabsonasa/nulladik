function solve(s)
  function freq(a)
    f=zeros(Int,128)
    for ai in a
      f[Int(ai)]+=1
    end
    f
  end
  f=freq(s)
  ls,lus=length(s),length(unique(s))
  mx,fmx,mn,fmn=0,0,ls+1,0
  for c in Int('a'):Int('z')
    if (fc=f[c])==0 continue end
    if fc>mx
      mx=fc
      fmx=1
    elseif fc==mx
      fmx+=1
    end
    if fc<mn
      mn=fc
      fmn=1
    elseif fc==mn
      fmn+=1
    end
  end
#println(f)
  mx==mn || ( mn==1 && fmx==lus-1 ) || ( mn+1==mx && fmn==lus-1 )
end

if solve(readline())
  println("YES")
else
  println("NO")
end