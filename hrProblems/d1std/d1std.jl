function solve()

  n=parse(Int,readline())
  x=parse.(Int,split(readline()))
  mu=sum(x)/n
  mu2=sum(x .* x)/n
  sig=sqrt(mu2-mu*mu)
  println(round(sig,digits=1))  
end

solve()