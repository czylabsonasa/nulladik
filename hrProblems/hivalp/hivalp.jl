function solve()
  n,k=parse.(Int,split(readline()))
  arr=parse.(Int, collect(readline()))
println(stderr,arr)
  volt=fill(false,n)
  i,j=1,n
  while k>=0 && i<j
    if arr[i]!=arr[j]
      k-=1
      volt[i]=true
      mx=max(arr[i],arr[j])
      arr[i],arr[j]=mx,mx
    end  
    i,j=i+1,j-1
  end
  if k<0
    println("-1")
    return
  end
  i,j=1,n
  while k>0 && i<j
    if arr[i]<9
      if volt[i] 
        arr[i],arr[j]=9,9; k-=1
      elseif k>1 
        arr[i],arr[j]=9,9; k-=2
      end
    end
    i,j=i+1,j-1
  end
  println(join(arr))
end
solve()