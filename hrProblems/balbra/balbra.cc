// Balanced Brackets
#include <bits/stdc++.h>

using namespace std;

int main(){
  auto nyit = [](int c){
    return(c == '(' || c == '[' ||c=='{');
  };
  auto par = [](int c){
    if(c==')')return '(';
    if(c==']')return '[';
    if(c=='}')return '{';
    return '_';// control reaches end of non void function
  };

  int Q;
  cin >> Q;
  while(Q--){
    string szo; cin>>szo;
    int nszo=szo.length();
    vector<char> st(nszo);
    int nst=0;
    for(auto c : szo){
      if( nyit(c) ){
        st[nst++]=c;
      }else{
        if( nst>0 && st[nst-1]==par(c)){
          nst--;
        }else{
          nst=-1; break;
        }
      }
    }
    if(0==nst){
      cout<<"YES\n";
    }else{
      cout << "NO\n";
    }
  }
  return 0;
}
