function solve(s)
  hs=length(s)
  if hs<2 return false,-1 end
  for h=1:div(hs,2)
    start=parse(Int,s[1:h])
    if s[1]=='0' && h>1 break end 
    ts=s[1:h]
    akt=start
    hts=h
    while hts<hs
      akt+=1
      sa=string(akt)
      ts=ts*sa
      hts+=length(sa)
    end
    if ts==s
      return true,start
    end
  end
  false,-1
end

esetek=parse(Int,readline())
for _ in 1:esetek
  oc,st=solve(readline())
  if true==oc
    println("YES ",st)
  else
    println("NO")
  end
end