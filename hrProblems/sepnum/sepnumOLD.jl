function solve(s)
  ns=length(s)
  for k=1:div(ns,2)
    start=parse(Int,s[1:k])
    if start==0 && k>1 break end 
    akt=start
    h=k 
    j=k+1
    while true
      if j==ns+1 return true,start end
      nxt=parse(Int,s[j:j+h-1])
      if nxt==akt+1
        akt+=1
        j+=h
        continue
      end
      if 10*nxt<akt && (nxt=10*nxt+parse(Int,s[j+h:j+h]))==akt+1
        akt+=1
        h+=1
        j+=h
        continue
      end
      break
    end
  end
  false,-1
end

esetek=parse(Int,readline())
for _ in 1:esetek
  oc,st=solve(readline())
  if true==oc
    println("YES ",st)
  else
    println("NO")
  end
end