# Complete the breakingRecords function below.
function breakingRecords(scores)
    nlo,nhi=0,0
    lo,hi=scores[1],scores[1]
    for s in scores
        if s>hi
            nhi+=1
            hi=s
            continue
        end
        if s<lo
            nlo+=1
            lo=s
            continue
        end

    end
    nlo,nhi
end

# fptr = open(ENV["OUTPUT_PATH"], "w")
fptr = open("out", "w")

n = parse(Int32, readline(stdin))

scores = [parse(Int32, k) for k in split(readline(stdin))]

result = breakingRecords(scores)

write(fptr, join(result, " "))
write(fptr, "\n")

close(fptr)
