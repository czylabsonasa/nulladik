# minimum height triangle

b,a=parse.(Int,split(readline()))
h,r=divrem(2*a,b)
println(h+if r>0 1 else 0 end)