# Mark and Toys
let
  n,k=parse.(Int,split(readline()))
  p=sort(parse.(Int,split(readline())))
  i=0
  while k>0 && i<n
    if k>=p[i+1]
      i+=1; k-=p[i]
    else
      break
    end
  end
  println(i)
end