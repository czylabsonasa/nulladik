function solve(s)
  arr=Int.(collect(s))
  narr=length(arr)
  i,j=1,narr
  while i<narr
    if abs(arr[i]-arr[i+1]) != abs(arr[j]-arr[j-1]) return false end
    i,j=i+1,j-1
  end
  true
end

esetek=parse(Int,readline())
for _ in 1:esetek
  if true != solve(readline()) print("Not ") end
  println("Funny")
end