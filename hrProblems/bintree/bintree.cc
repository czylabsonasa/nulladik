// Trees: Is This a Binary Search Tree?
/*
The Node struct is defined as follows:
	struct Node {
		int data;
		Node* left;
		Node* right;
	}
*/
struct info{int res,mn,mx;};
info trav(Node* root){
  if(nullptr==root){return info{0,0,0};}
  int data=root->data;
  int mn=data, mx=data, res=0;  
  if(nullptr!=(root->left) && (root->left)->data>=data){return info{-1,0,0};}
  auto t=trav(root->left);
  if(t.res<0){return info{-1,0,0};}
  if(t.res>0){
    if(t.mx>=data){return info{-1,0,0};}
    mn=t.mn;
  }
  if(nullptr!=(root->right) && (root->right)->data<=data){return info{-1,0,0};}
  t=trav(root->right);
  if(t.res<0){return info{-1,0,0};}
  if(t.res>0){
    if(t.mn<=data){return info{-1,0,0};}
    mx=t.mx;
  }
  return info{1,mn,mx};
}
bool checkBST(Node* root){
  return trav(root).res>=0;
}
