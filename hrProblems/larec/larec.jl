function solve()
  n=1+parse(Int,readline())
  h=vcat(parse.(Int,split(readline())),0)
  sta=[[0,0] for i in 1:n]
# println(stderr,h)
  nsta=1
  opt=0
  for i in 1:n
#println(stderr,sta[1:nsta])
    akt,accu=h[i],0
    while true
      top=sta[nsta]
      if akt>=top[1] break end
      opt=max(opt,top[1]*(accu+top[2]))
      accu+=top[2]  
      nsta-=1
    end
    if akt>sta[nsta][1]
      nsta+=1
      sta[nsta]=[akt,1+accu]
    else
      sta[nsta][2]+=(1+accu)
    end
  end
  println(opt)
end

solve()
