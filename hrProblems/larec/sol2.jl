function largestsquare(h::Array{Int})
    if length(h) == 0
        return 0
    elseif length(h) == 1
        return h[1]
    else
        _,smallest = findmin(h)
        biggest = length(h) * h[smallest]
        a = largestsquare(h[1:smallest-1])
        b = largestsquare(h[smallest+1:end])
        return max(biggest, a, b)
    end
end
readline()
houses = parse.(Int,split(readline()))
#[eval(parse(e))::Int for e in split(chomp(readline()), ' ')]
println(largestsquare(houses))