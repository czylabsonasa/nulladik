# sherlock and anagrams
let
  frek=zeros(Int,128)

  T=parse(Int,readline())
  for _ in 1:T
    szo=strip(readline())
    nszo=length(szo)

    ans=0
    for h=1:nszo-1
      frek[Int('a'):Int('z')] .= 0
      for c in 1:h frek[Int(c)]+=1 end
      d=Dict{Array{Int,1},Int}()
      d[frek[Int('a'):Int('z')]]=1
      for j in h+1:nszo
        frek[Int(szo[j])]+=1
        frek[Int(szo[j-h])]-=1
        arr=frek[Int('a'):Int('z')]
        d[arr] = if haskey(d,arr) d[arr]+1 else 1 end
      end
      for (k,v) in d
        ans += if v>1 div(v*(v-1),2) else 0 end
      end
    end
    println(ans)
  end

end
