

## Témák:
1. ### [prímszámok](https://hu.wikipedia.org/wiki/Pr%C3%ADmsz%C3%A1mok)
1. ### egyedi prímszámteszt
1. ### Eratoszthenész szitája

### prímszámok
**Röviden:** Legyen az univerzumunk az egytől
nagyobb pozitív egészek halmaza. <br>
Vannak olyan számok melyeknek az
**1**-en és önmagukon kívül nincs más osztójuk.<br>
Nevezzük ezeket prímnek a többit összetettnek.
(Az **1** sem nem prím, sem nem összetett.)<br>

**Egymondatban:** egytől nagyobb **n** szám pontosan akkor nem prím,
ha van **2** és **n−1** között osztója.


### egyedi prímszámteszt
#### Feladat:
Egy függvény megvalósítása mely eldönti a paraméterül kapott számról, hogy prím-e (```true```) vagy sem (```false```)

#### Megoldás:
```julia
function isPrime0(n)
  if n<2 return false end
  for d in 2:(n-1)
    if 0==mod(n,d) return false end
  end
  true
end
```

#### Új ismeret:
```div``` és ```mod``` használata.


### Használat:
```julia
for n in 1:10
  println(n,":",isPrime0(n))
end
```

#### Finomabb megoldás:
```julia
function isPrime1(n)
  if n<5 return (false,true,true,false)[n] end
  if 0==mod(n,2) || 0==mod(n,3) return false end
  d,delta=5,2
  while d*d<=n # itt nekünk kell kezelni a ciklusváltozót
    if 0==mod(n,d) return false end
    d+=delta
    delta=6-delta
  end
  true
end
```

#### Háttér:
* Megpróbáljuk kevesebb számra tesztelni, hogy osztója-e **n**-nek:
   * Egy **4**-től nagyobb prímszám **1** vagy **5** maradékot ad **6**-tal osztva, <br>
   ezért csak ezeken megyünk végig.
* Egy összetett számnak biztosan van olyan osztója mely nem nagyobb mint $`\sqrt{n}`$, <br>azaz ettől nagyobb számra nem kell tesztelni.

### Eratoszthenész szitája
Előfordul, hogy egy viszonylag kicsi intervallum **összes** számjáról el akarjuk dönteni, hogy prím-e vagy sem, esetleg többször is egy probléma megoldása során. Ekkor az egyedi tesztelés helyett alkalmazhatjuk a szitálást. Ha az $[1,n]$ intervallumot akarjuk feldolgozni akkor alkalmazzuk  következőt:
1. Inicializálás:<br> ```szita[ 1:n ] = true; szita[ 1 ] = false```
1. Induljunk el ```p```-vel ```2```-tól ```n```-ig. Ha ```szita[p]``` igaz, akkor a  ```p``` minden többszörösét (```2p,3p,...```) szitáljuk ki a tömbből.
Az elgoritmus futása során a meglátogatott ```p``` számok két csoportra oszthatók:
1. ```false==szita[p]``` ezen számokat egy előttelevő számmal kiszitáltunk, így összetettek.
1. ```true==szita[p]``` ezek az érintetlen számok prímek. Ezt beláthatjuk teljes indukcióval. Állítás:
a szita futása során minden ```p```-re igaz, hogy amikor ```p```-nél járunk, akkor az ```[1,p-1]``` intervallum összes számát jól azonosította a módszer.
Ez ```p=2,3```-ra igaz. Tegyuk fel, hogy igaz az állítás valamely ```q=p```-re. Ekkor, ha ```true==szita[q+1]``` akkor ha ```q+1``` összetett lenne, akkor a feltevés szerint jól azonosított tőle kisebb számmal kiszitáltuk volna, ez ellentmondás.
