### Bevezető feladat
Járjuk be az $`n`$ szintből álló, teljes bináris fát!
**vagy ami ugyanaz:**
generáljuk le (írjuk ki az ```stdout```-ra) az összes adott $`n`$ hosszúságú ```0-1``` sorozatot!

#### Megoldás:
rekurzió: minden szinten mindent végigpróbálunk és visszalépünk ha elfogytak a lehetőségek.

```julia
function gen01( hossz; ír=true )
  akt = fill( '_',hossz ) # -> fill
  function bejár( szint )
    if szint > hossz
      ( true == ír ) ? println( join(akt) ) : nothing
      return
    end
    akt[ szint ] = '0'
    bejár( 1+szint )
    akt[ szint ] = '1'
    bejár( 1+szint )
  end
end
@time gen01(3,ír=false)(1)
```







#### Julia:
1. A fenti függvény egy függvényt ad vissza. Így kezelhetők például a globális vaétozók.
1. ```fill, join```<br>
1. [ opcionális paraméterek ]( https://docs.julialang.org/en/v1/manual/functions/index.html#Optional-Arguments-1 )<br>
1. [ keyword paraméterek ](https://docs.julialang.org/en/v1/manual/functions/index.html#Keyword-Arguments-1)
