### [Euler projekt](https://projecteuler.net/)

#### Feladatok:
1. [Multiples of 3 and 5](projecteuler/1.md)
1. [Even Fibonacci numbers](projecteuler/2.md)
1. [Largest prime factor](projecteuler/3.md)
1. [Largest palindrome product](projecteuler/4.md)
1. [Smallest multiple](projecteuler/5.md)
1. [Sum square difference](projecteuler/6.md)
1. [10001st prime](projecteuler/7.md)
1. [Special Pythagorean triplet](projecteuler/9.md)
