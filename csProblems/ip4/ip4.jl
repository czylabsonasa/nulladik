function isIPv4Address(inp::String)
  sp=split(inp,'.') #; println(sp)
  if length(sp)!=4 || any(length.(sp) .== 0) return false end
  for s in sp
    if !all(isdigit.(collect(s))) || !(0<=parse(Int,s)<=255)
      return false
    end
  end
  true
end
println(isIPv4Address(readline()))