function areSimilar(a, b)
  function go(i,n)
    while i<=n && a[i]==b[i] 
      i+=1 
    end
    i
  end
  n=length(a)
  i=go(1,n)
  if i>n return true end
  ai,bi=a[i],b[i]
  i=go(i+1,n)
  if i>n || (a[i]!=bi || b[i]!=ai) false else go(i+1,n)>n end
end
 println(areSimilar([1,4,2], [2,4,1]))
