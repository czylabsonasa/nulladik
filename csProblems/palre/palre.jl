function palindromeRearranging(inp)
  frek=zeros(Int,128)
  for c in inp frek[Int(c)]+=1 end
  no=0
  for c in Int('a'):Int('z') no+=(frek[c]%2) end
  0==no || (1==no && 1==length(inp)%2)
end
println(palindromeRearranging(readline()))